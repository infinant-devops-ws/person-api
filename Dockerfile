FROM adoptopenjdk/openjdk13:alpine

WORKDIR /opt
COPY build/libs/person-api-1.0.jar /opt

ENTRYPOINT java -jar person-api-1.0.jar

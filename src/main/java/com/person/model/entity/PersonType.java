package com.person.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "person_type")
public class PersonType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idperson_type")
    private int idPersonType;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "personType", cascade = CascadeType.ALL)
    private List<Person> persons;
}

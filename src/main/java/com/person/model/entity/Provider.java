package com.person.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "provider")
public class Provider {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idprovider")
    private int idprovider;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
    private List<Person> persons;
}

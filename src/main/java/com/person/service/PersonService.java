package com.person.service;

import com.person.model.dto.PersonDTO;
import com.person.model.entity.Person;

import java.util.List;
import java.util.Optional;

public interface PersonService {

    void savePerson(PersonDTO personDTO);
    Optional<List<Person>> getAllClients();
}

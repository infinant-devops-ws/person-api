package com.person.service;

import com.person.model.entity.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService {

    void saveRole(Role role);
    Optional<List<Role>> getAllRoles();
    void deleteRole(Role role);
}

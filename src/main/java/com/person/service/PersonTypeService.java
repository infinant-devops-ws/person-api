package com.person.service;

import com.person.model.entity.PersonType;

import java.util.List;
import java.util.Optional;

public interface PersonTypeService {

    void savePersonType(PersonType personType);
    Optional<List<PersonType>> getAllPersonTypes();
    void deletePersonType(PersonType personType);
}

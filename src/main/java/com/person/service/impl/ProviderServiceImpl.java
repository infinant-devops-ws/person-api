package com.person.service.impl;

import com.person.dao.ProviderDAO;
import com.person.model.entity.Provider;
import com.person.service.ProviderService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProviderServiceImpl implements ProviderService {

    final ProviderDAO dao;

    public ProviderServiceImpl(ProviderDAO dao) {
        this.dao = dao;
    }

    @Override
    public void saveProvider(Provider provider) {
        dao.save(provider);
    }

    @Override
    public Optional<List<Provider>> getAllProviders() {
        return Optional.of(dao.findAll());
    }

    @Override
    public void deleteProvider(Provider provider) {
        dao.delete(provider);
    }
}

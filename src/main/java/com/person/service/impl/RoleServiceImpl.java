package com.person.service.impl;

import com.person.dao.RoleDAO;
import com.person.model.entity.Role;
import com.person.service.RoleService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    final RoleDAO dao;

    public RoleServiceImpl(RoleDAO dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public void saveRole(Role role) {
        dao.save(role);
    }

    @Override
    public Optional<List<Role>> getAllRoles() {
        return Optional.of(dao.findAll());
    }

    @Override
    @Transactional
    public void deleteRole(Role role) {
        dao.delete(role);
    }
}

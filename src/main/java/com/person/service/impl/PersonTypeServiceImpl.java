package com.person.service.impl;

import com.person.dao.PersonTypeDAO;
import com.person.model.entity.PersonType;
import com.person.service.PersonTypeService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PersonTypeServiceImpl implements PersonTypeService {

    final PersonTypeDAO dao;

    public PersonTypeServiceImpl(PersonTypeDAO dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public void savePersonType(PersonType personType) {
        dao.save(personType);
    }

    @Override
    public Optional<List<PersonType>> getAllPersonTypes() {
        return Optional.of(dao.findAll());
    }

    @Override
    @Transactional
    public void deletePersonType(PersonType personType) {
        dao.delete(personType);
    }
}

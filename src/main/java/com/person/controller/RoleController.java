package com.person.controller;

import com.person.model.entity.Role;
import com.person.service.RoleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/role")
public class RoleController {

    final RoleService service;

    public RoleController(RoleService service) {
        this.service = service;
    }

    @PostMapping(path = "/add")
    public ResponseEntity<?> saveRole(@RequestBody Role role) {
        service.saveRole(role);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping(path = "/list")
    public ResponseEntity<?> getAllRoles() {
        Optional<List<Role>> response = service.getAllRoles();
        return ResponseEntity.ok(response.get());
    }

    @DeleteMapping(path = "/delete")
    public ResponseEntity<?> deleteRole(@RequestBody Role role) {
        service.deleteRole(role);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}

package com.person.controller;

import com.person.model.entity.Provider;
import com.person.service.ProviderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/provider")
public class ProviderController {

    final ProviderService service;

    public ProviderController(ProviderService service) {
        this.service = service;
    }

    @PostMapping(path = "/add")
    public ResponseEntity<?> savePersonType(@RequestBody Provider provider) {
        service.saveProvider(provider);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping(path = "/list")
    public ResponseEntity<?> getAllPersonTypes() {
        Optional<List<Provider>> response = service.getAllProviders();
        return ResponseEntity.ok(response.get());
    }

    @DeleteMapping(path = "/delete")
    public ResponseEntity<?> deletePersonType(@RequestBody Provider provider) {
        service.deleteProvider(provider);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}

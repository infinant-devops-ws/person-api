package com.person.controller;

import com.person.model.dto.PersonDTO;
import com.person.model.entity.Person;
import com.person.service.PersonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/person")
public class PersonController {

    final PersonService service;

    public PersonController(PersonService personService) {
        this.service = personService;
    }

    @PostMapping(path = "/add")
    public ResponseEntity<?> savePerson(@RequestBody PersonDTO personDTO) {
        service.savePerson(personDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping(path = "/clients")
    public ResponseEntity<?> getAllClients() {
        Optional<List<Person>> response = service.getAllClients();
        return ResponseEntity.ok(response.get());
    }
}

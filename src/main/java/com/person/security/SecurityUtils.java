package com.person.security;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Component
public class SecurityUtils {

    public String getTokenFromRequest(HttpServletRequest request) {
        Optional<Cookie> cookieOptional = Optional.ofNullable(WebUtils.getCookie(request, "token"));
        Optional<String> bearerTokenOptional = Optional.ofNullable(request.getHeader("Authorization"));

        if (cookieOptional.isPresent()) {
            return cookieOptional.stream().filter(cookie -> cookie.getValue().length() > 0)
                    .map(cookie -> cookie.getValue()).findFirst().orElse(null);
        } else if (bearerTokenOptional.isPresent()) {
            return bearerTokenOptional.stream().filter(bearerToken -> bearerToken.length() > 0)
                    .filter(bearerToken -> StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer"))
                    .map(bearerToken -> bearerToken.substring(7, bearerToken.length())).findFirst().orElse(null);
        }

        return null;
    }
}

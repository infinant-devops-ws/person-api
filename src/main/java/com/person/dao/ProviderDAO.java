package com.person.dao;

import com.person.model.entity.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProviderDAO extends JpaRepository<Provider, Integer> {

    Optional<Provider> findByName(String name);
}

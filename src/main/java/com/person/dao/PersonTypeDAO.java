package com.person.dao;

import com.person.model.entity.PersonType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonTypeDAO extends JpaRepository<PersonType, Integer> {

    Optional<PersonType> findByName(String name);
}

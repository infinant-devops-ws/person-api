package com.person.dao;

import com.person.model.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonDAO extends JpaRepository<Person, String> {

    Optional<List<Person>> findByPersonType_IdPersonType(int idPersonType);
}
